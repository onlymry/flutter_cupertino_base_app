import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../components/CupertinoNavBar.dart';

class Search extends StatefulWidget {
  Search({Key key}) : super(key: key);
  SearchBase createState() => SearchBase();
}

class SearchBase extends State<Search> {
  Widget build(context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavBar(
        heroTag: "Serach",
        actionsForegroundColor: Color(0xfff000000),
        middle: Text('Search'),
      ),
      child: Container(),
    );
  }
}
