import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Brands extends StatefulWidget {
  Brands({Key key}) : super(key: key);
  BrandsBase createState() => BrandsBase();
}

class BrandsBase extends State<Brands> {
  Widget build(context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        heroTag: "Brands",
        transitionBetweenRoutes: false,
        middle: Text('Brands'),
      ),
      child: Container(),
    );
  }
}
