import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Cart extends StatefulWidget {
  Cart({Key key}) : super(key: key);
  CartBase createState() => CartBase();
}

class CartBase extends State<Cart> {
  Widget build(context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        heroTag: 'Cart',
        transitionBetweenRoutes: false,
        middle: Text('Cart'),
      ),
      child: Container(),
    );
  }
}
