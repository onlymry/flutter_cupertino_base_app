import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../components/CupertinoNavBar.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);
  HomeBase createState() => HomeBase();
}

class HomeBase extends State<Home> {
  /*
  * 点击搜索按钮导航到搜索页面
  */
  void _searchEvent(context) {
    print('search');
    Navigator.of(context).pushNamed('/search');
  }

  Widget build(context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavBar(
          heroTag: "Home",
          transitionBetweenRoutes: false,
          middle: Text('Home'),
          leading: GestureDetector(
              child: Icon(
                IconData(0xe689, fontFamily: 'tbicon'),
                color: Color(0xfff000000),
              ),
              onTap: () {
                print('scan');
              }),
          trailing: GestureDetector(
            child: Icon(
              IconData(0xe65c, fontFamily: 'tbicon'),
              color: Color(0xfff000000),
            ),
            onTap: () {
              this._searchEvent(context);
            },
          )),
      child: Container(),
    );
  }
}
